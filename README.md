cf46-0a9a.rhpds.opentlc.com  
tower-0a9a.rhpds.opentlc.com  

Using 3 Steps provisioning on CFME in order to use Ansible for PreProv Tasks and PostProv Tasks:
- Step 1: Catalog Item calling Ansible to PreProv Tasks (IPAM, AD, ...)
- Step 2: Catalog Item calling the Provider with the proper options (Env, Cluster, VLAN....)
- Step 3: Catalog Item Calling Ansible to PostProv Tasks (Register IPAM, Satellite, Install Software)

Abstract:

The idea is creating a full provisioning Workflow, using Ansible to Automate all the tasks (preprov and postprov) but still being able to use the CFME OOTB functionalities like approval,quotas,retirement,notification,Service structure....

Ansible will call back CFME to provision the VM, so all the pre/post prov task can be reused independently, due to the only link with the provider will be the Catalog item in the step 2 that can be replaced in order to use any provider supported by CFME.

Outcome:
-Single Dialog where we can select any provider, but still reusing the same Ansible code to preconfigure and postconfigure the VM and all related systems (IPAM, AD, Puppet, Satellite, CMDB....)

Future:
- Multi VM deployments
- Ansible Retirement for Tower and Inside
- Multi Service Linking

LINKS :

GIT : https://gitlab.com/redhat-cop/EMEACloudFormsHackathon/cfme3stepsansibleprovisioning

PROJECT : https://gitlab.com/redhat-cop/EMEACloudFormsHackathon/introduction/issues/25
